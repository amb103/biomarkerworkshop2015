\documentclass[compress]{beamer}
\usetheme{Warsaw}
%\usepackage[utf8]{inputenc}
%\usepackage{default}
\setbeamertemplate{footline}[text line]{}
\useinnertheme{circles}
\usepackage{url}
\usepackage[export]{adjustbox}
\usepackage{tikz}
\usetikzlibrary{arrows,shapes,snakes,automata,backgrounds,fit,petri}
\definecolor{imp}{rgb}{0.76,0.242,0.00}
\definecolor{nimp}{RGB}{132,132,143}
\definecolor{bc1}{RGB}{26,37,57}
\definecolor{bc2}{RGB}{36,107,155}
\newcommand{\imp}[1]{{\color{imp} #1}}
\newcommand{\nimp}[1]{{\color{nimp} #1}}
\setbeamercolor*{palette primary}{use=structure,fg=white,bg=bc1}
\setbeamercolor*{palette quaternary}{fg=white,bg=bc2} 
\usepackage[english]{babel}
\usepackage[applemac]{inputenc}
\usepackage{times}
\usepackage[T1]{fontenc}
\usepackage{listings}
\usepackage{color}
\lstset{ %
  language=R,                     % the language of the code
  basicstyle=\tiny,       % the size of the fonts that are used for the code
  numbers=left,                   % where to put the line-numbers
  numberstyle=\tiny\color{green},  % the style that is used for the line-numbers
  stepnumber=1,                   % the step between two line-numbers. If it's 1, each line
                                  % will be numbered
  numbersep=5pt,                  % how far the line-numbers are from the code
  backgroundcolor=\color{white},  % choose the background color. You must add \usepackage{color}
  showspaces=false,               % show spaces adding particular underscores
  showstringspaces=false,         % underline spaces within strings
  showtabs=false,                 % show tabs within strings adding particular underscores
  frame=single,                   % adds a frame around the code
  rulecolor=\color{black},        % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. commens (green here))
  tabsize=2,                      % sets default tabsize to 2 spaces
  captionpos=b,                   % sets the caption-position to bottom
  breaklines=true,                % sets automatic line breaking
  breakatwhitespace=false,        % sets if automatic breaks should only happen at whitespace
  title=\lstname,                 % show the filename of files included with \lstinputlisting;
                                  % also try caption instead of title
  keywordstyle=\color{blue},      % keyword style
  commentstyle=\color{gray},   % comment style
  stringstyle=\color{mauve},      % string literal style
  escapeinside={\%*}{*)},         % if you want to add a comment within your code
  morekeywords={*,...}            % if you want to add more keywords to the set
} 

\title{ { Biomarker Discovery Workshop }}
\subtitle{ Module 4: Hypothesis Testing }
\author{\small Instructor: Ashlee Valente}
\institute{Center for Applied Genomics and Precision Medicine}
\date[ uh ]{\small February 16, 2016}

\begin{document}

\begin{frame}
	\titlepage
%	\includegraphics[height = 0.75cm,center]{images/both_logo.png}
\end{frame}


\begin{frame}{Workshop Overview}
 \centering
\includegraphics[scale=0.17]{images/StatsWorkshop.pdf}
\end{frame}

\section{Examples}
\begin{frame}[fragile]{One-Sample T-Test}
\begin{lstlisting}[language=R]
	
	# Ex. An outbreak of Salmonella-related illness was attributed to ice cream produced 
	# at a certain factory. Scientists measured the level of Salmonella in 10 randomly
	# sampled batches of ice cream. The levels (in MPN/g) were:
	# 0.593 0.142 0.329 0.691 0.231 0.793 0.519 0.392 0.418 0.568

	# Is there evidence that the mean level of Salmonella in the ice cream is greater
	# than 0.3 MPN/g?
	# Let x be the mean level of Salmonella in all batches of ice cream. Here the
	# hypothesis of interest can be expressed as:
	# H0: x = 0.3 
	# Ha: x > 0.3

	x = c(0.593, 0.142, 0.329, 0.691, 0.231, 0.793, 0.519, 0.392, 0.418, 0.568)
	
	# We can test for normality with a Shapiro Wilk Test
	shapiro.test(rnorm(100, mean = 5, sd = 3)) # What happens with normally-distributed data
	shapiro.test(runif(100, min = 2, max = 4)) # What happens with non-normally-distributed data
	
	# Can we reasonably assume normality?
	shapiro.test(x)
	
	# Perform the one sample test
	t.test(x, alternative="greater", mu=0.3, conf.level = 0.95)
	
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{One-Sample T-Test}
\begin{lstlisting}[language=R]
	
	> shapiro.test(rnorm(100, mean = 5, sd = 3)) # What does this return for normally-distributed data

		Shapiro-Wilk normality test

	data:  rnorm(100, mean = 5, sd = 3)
	W = 0.9925, p-value = 0.8571

	> shapiro.test(runif(100, min = 2, max = 4)) # What does this return for non-normally-distributed data

		Shapiro-Wilk normality test

	data:  runif(100, min = 2, max = 4)
	W = 0.9562, p-value = 0.002184
	
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{One-Sample T-Test}
\begin{lstlisting}[language=R]
	
	> # Can we reasonably assume normality?
	> shapiro.test(x)

		Shapiro-Wilk normality test

	data:  x
	W = 0.9864, p-value = 0.9901

	> # Perform the one sample test
	> t.test(x, alternative="greater", mu=0.3, conf.level = 0.95)

		One Sample t-test

	data:  x
	t = 2.6012, df = 9, p-value = 0.01434
	alternative hypothesis: true mean is greater than 0.3
	95 percent confidence interval:
	 0.3494909       Inf
	sample estimates:
	mean of x 
	   0.4676 
	
\end{lstlisting}
\end{frame}


\begin{frame}[fragile]{Two-Sample T-Test}
\begin{lstlisting}[language=R]
	
# Ex. Six subjects were given a drug (treatment group) and an additional six
# subjects a placebo (control group). Their reaction time to a stimulus was
# measured (in ms). We want to perform a two-sample t-test for comparing the
# means of the treatment and control groups.
# Let x be the mean of the population taking medicine and y the mean of the
# untreated population. Here the hypothesis of interest can be expressed as:
# H0: x-y=0 
# Ha: x-y<0

control = c(91, 87, 99, 77, 88, 91)
treat = c(101, 110, 103, 93, 99, 104)

# Can we reasonably assume normality?
shapiro.test(control)
shapiro.test(treat)

# Are the variances equal?
var.test(control,treat) # (This is an F test)

# Perform the two sample test assuming equal variance
t.test(control,treat,alternative="less", var.equal=TRUE, conf.level = 0.95)
	
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Two-Sample T-Test}
\begin{lstlisting}[language=R]
	
	> # Can we reasonably assume normality?
	> shapiro.test(control)

		Shapiro-Wilk normality test

	data:  control
	W = 0.9394, p-value = 0.6545

	> shapiro.test(treat)

		Shapiro-Wilk normality test

	data:  treat
	W = 0.9823, p-value = 0.9624

	> # Are the variances equal?
	> var.test(control,treat) # (This is an F test)

		F test to compare two variances

	data:  control and treat
	F = 1.6119, num df = 5, denom df = 5, p-value = 0.6131
	alternative hypothesis: true ratio of variances is not equal to 1
	95 percent confidence interval:
	  0.2255582 11.5194293
	sample estimates:
	ratio of variances 
	          1.611925 

\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Two-Sample T-Test}
\begin{lstlisting}[language=R]

	> # Perform the two sample test assuming equal variance
	> t.test(control,treat,alternative="less", var.equal=TRUE, conf.level = 0.95)

		Two Sample t-test

	data:  control and treat
	t = -3.4456, df = 10, p-value = 0.003136
	alternative hypothesis: true difference in means is less than 0
	95 percent confidence interval:
	      -Inf -6.082744
	sample estimates:
	mean of x mean of y 
	 88.83333 101.66667 
	
	
\end{lstlisting}
\end{frame}


\begin{frame}[fragile]{Paired T-Test}
\begin{lstlisting}[language=R]
	
# Ex. A study was performed to test whether cars get better mileage on premium
# gas than on regular gas. Each of 10 cars was first filled with either regular
# or premium gas, decided by a coin toss, and the mileage for that tank was
# recorded. The mileage was recorded again for the same cars using the other
# kind of gasoline. We use a paired t-test to determine whether cars get
# significantly better mileage with premium gas.

reg = c(16, 20, 21, 22, 23, 22, 27, 25, 27, 28)
prem = c(19, 22, 24, 24, 25, 25, 26, 26, 28, 32)

# Can we reasonably assume normality?
shapiro.test(reg)
shapiro.test(prem)

# Perform the paired t-test
t.test(prem,reg,alternative="greater", paired=TRUE, conf.level = 0.95)
	
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Paired T-Test}
\begin{lstlisting}[language=R]
	
# Can we reasonably assume normality?
> shapiro.test(reg)

	Shapiro-Wilk normality test

data:  reg
W = 0.9481, p-value = 0.6459

> shapiro.test(prem)

	Shapiro-Wilk normality test

data:  prem
W = 0.9555, p-value = 0.7332

> # Perform the paired t-test
> t.test(prem,reg,alternative="greater", paired=TRUE, conf.level = 0.95)

	Paired t-test

data:  prem and reg
t = 4.4721, df = 9, p-value = 0.0007749
alternative hypothesis: true difference in means is greater than 0
95 percent confidence interval:
 1.180207      Inf
sample estimates:
mean of the differences 
                      2 
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Wilcoxon Rank Sum Test}
\begin{lstlisting}[language=R]
	
## Two-sample test.
## Hollander & Wolfe (1973), 69f.
## Permeability constants of the human chorioamnion (a placental
##  membrane) at term (x) and between 12 to 26 weeks gestational
##  age (y).  The alternative of interest is greater permeability
##  of the human chorioamnion for the term pregnancy.

x <- c(0.80, 0.83, 1.89, 1.04, 1.45, 1.38, 1.91, 1.64, 0.73, 1.46)
y <- c(1.15, 0.88, 0.90, 0.74, 1.21)

wilcox.test(x, y, alternative = "greater")

\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Wilcoxon Rank Sum Test}
\begin{lstlisting}[language=R]
	
> wilcox.test(x, y, alternative = "greater")

	Wilcoxon rank sum test

data:  x and y
W = 35, p-value = 0.1272
alternative hypothesis: true location shift is greater than 0

\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Wilcoxon Paired Test}
\begin{lstlisting}[language=R]
	
## Hollander & Wolfe (1973), 29f.
## Hamilton depression scale factor measurements in 9 patients with
##  mixed anxiety and depression, taken at the first (x) and second
##  (y) visit after initiation of a therapy (administration of a
##  tranquilizer).

x = c(1.83,  0.50,  1.62,  2.48, 1.68, 1.88, 1.55, 3.06, 1.30)
y = c(0.878, 0.647, 0.598, 2.05, 1.06, 1.29, 1.06, 3.14, 1.29)

wilcox.test(x, y, paired = TRUE, alternative = "greater")

\end{lstlisting}
\end{frame}


\begin{frame}[fragile]{Wilcoxon Paired Test}
\begin{lstlisting}[language=R]
	
> wilcox.test(x, y, paired = TRUE, alternative = "greater")

	Wilcoxon signed rank test

data:  x and y
V = 40, p-value = 0.01953
alternative hypothesis: true location shift is greater than 0


\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{ANOVA}
\begin{lstlisting}[language=R]
	
# Ex. A drug company tested three formulations of a pain relief medicine
# for migraine headache sufferers. For the experiment 27 volunteers were
# selected and 9 were randomly assigned to one of three drug formulations.
# The subjects were instructed to take the drug during their next migraine
# headache episode and to report their pain on a scale of 1 to 10 (10
# being most pain).

pain = c(4, 5, 4, 3, 2, 4, 3, 4, 4, 6, 8, 4, 5, 4, 6, 5, 8, 6, 6, 7, 6, 6, 7, 5, 6, 5, 5)
drug = c(rep("A",9), rep("B",9), rep("C",9))
migraine = data.frame(pain,drug)

# Note the command rep("A",9) constructs a list of nine A's in a row. The variable
# drug is therefore a list of length 27 consisting of nine A's followed by nine B's
# followed by nine C's.

# If we print the data frame migraine we can see the format the data should be on
# in order to make side-by-side boxplots and perform ANOVA.
migraine

# Make a quick boxplot
plot(pain ~ drug, data=migraine)

# Perform the ANOVA
results = aov(pain ~ drug, data=migraine)
summary(results)

\end{lstlisting}
\end{frame}


\begin{frame}[fragile]{ANOVA}
\begin{lstlisting}[language=R]

# If we print the data frame migraine we can see the format the data should be on
# in order to make side-by-side boxplots and perform ANOVA.
> migraine
   pain drug
1     4    A
2     5    A
3     4    A
4     3    A
5     2    A
6     4    A
7     3    A
8     4    A
9     4    A
10    6    B
11    8    B
12    4    B
13    5    B
14    4    B
15    6    B
16    5    B
17    8    B
18    6    B
19    6    C
20    7    C
21    6    C
22    6    C
23    7    C
24    5    C
25    6    C
26    5    C
27    5    C

\end{lstlisting}
\end{frame}


\begin{frame}[fragile]{ANOVA}
\begin{lstlisting}[language=R]
	
	# Make a quick boxplot
	plot(pain ~ drug, data=migraine)

\end{lstlisting}

\centering
\includegraphics[scale=0.35]{images/drugs.png}


\end{frame}

\begin{frame}[fragile]{ANOVA}
\begin{lstlisting}[language=R]
	
> # Perform the ANOVA
> results = aov(pain ~ drug, data=migraine)
> summary(results)
            Df Sum Sq Mean Sq F value   Pr(>F)    
drug         2  28.22  14.111   11.91 0.000256 ***
Residuals   24  28.44   1.185                     
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Chi-Squared}
\begin{lstlisting}[language=R]
	
# In this example, we seek to determine whether or not there is an association between gender and preference for ice cream flavor – these are the 2 categorical variables of interest.

# Gender has 2 categories: Men, Women
# Ice cream flavor has 3 categories: Chocolate, Vanilla, Strawberry
# The data come from a hypothetical survey of 920 people that ask
# for their preference of 1 of the above 3 ice cream flavours. 
# Here are the data:

#  	 	| Chocolate	|	Vanilla	|	Strawberry	|	Total
# Men	|	100	|	120	|	60	|	280
# Women	|	350	|	200	|	90	|	640
# Total	|	450	|	320	|	150	|	920

men = c(100, 120, 60)
women = c(350, 200, 90)
# place the occurence data in a data frame
ice.cream.survey = as.data.frame(rbind(men, women))

# assign column names
names(ice.cream.survey) = c('chocolate', 'vanilla', 'strawberry')
ice.cream.survey

# Test if flavor preference and gender are independent
chisq.test(ice.cream.survey)

\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Chi-Squared}
\begin{lstlisting}[language=R]
	
> ice.cream.survey
      chocolate vanilla strawberry
men         100     120         60
women       350     200         90
> 
> # Test if flavor preference and gender are independent
> chisq.test(ice.cream.survey)

	Pearson's Chi-squared test

data:  ice.cream.survey
X-squared = 28.3621, df = 2, p-value = 6.938e-07

\end{lstlisting}
\end{frame}


\begin{frame}[fragile]{Fisher's Exact Test}
\begin{lstlisting}[language=R]
	
## Agresti (1990, p. 61f; 2002, p. 91) Fisher's Tea Drinker
## A British woman claimed to be able to distinguish whether milk or
##  tea was added to the cup first.  To test, she was given 8 cups of
##  tea, in four of which milk was added first.  The null hypothesis
##  is that there is no association between the true order of pouring
##  and the woman's guess, the alternative that there is a positive
##  association (that the odds ratio is greater than 1).

teatasting = matrix(c(3, 1, 1, 3), nrow = 2,  dimnames = list(guess = c("Milk", "Tea"),truth = c("Milk", "Tea")))

#Look at the table
teatasting

#Test is there is an association between the true order and the woman's guess
fisher.test(teatasting, alternative = "greater")

\end{lstlisting}
\end{frame}



\begin{frame}[fragile]{Fisher's Exact Test}
\begin{lstlisting}[language=R]

#Look at the table
> teatasting
      truth
guess  Milk Tea
  Milk    3   1
  Tea     1   3

#Test is there is an association between the true order and the woman's guess
> fisher.test(teatasting, alternative = "greater")

	Fisher's Exact Test for Count Data

data:  teatasting
p-value = 0.2429
alternative hypothesis: true odds ratio is greater than 1
95 percent confidence interval:
 0.3135693       Inf
sample estimates:
odds ratio 
  6.408309 
  
\end{lstlisting}
\end{frame}

\section{Gene Expression Data}

\begin{frame}[fragile]{Install and Load some Libraries}
\begin{lstlisting}[language=R]
	
###### Install some libraries ######
install.packages("xlsx")
source("http://bioconductor.org/biocLite.R")
biocLite("Biobase")
biocLite("affy")
biocLite("limma")
biocLite("gcrma")

###### Load some libraries ######
library(Biobase)
library(affy)
library(xlsx)
library(gcrma)
library(limma)

\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Clean Up the Key}
\begin{lstlisting}[language=R]
	
###### Clean Up the Key and Merge RIN ######

setwd("/Users/ashlee/Box Sync/Home Folder amb103/Private/BiomarkerDiscoveryWorkshop/Data/3280_crypto_murine/")

# Read the key
key = read.table("Cryptokey.csv",skip=1,sep=",",header=TRUE)

# Fix the file names
key$CEL.file.name = paste(key$CEL.file.name,".CEL",sep="")
key$CEL.file.name = gsub("control", "controlm", key$CEL.file.name, perl=TRUE)

# Make a reasonable variable name
names(key)[3] = "mouse"

# Read the RNA quality information
qual = read.xlsx("12130_TotalRNAQC-Summary.xlsx",sheetIndex = 1,startRow = 3,endRow = 33)

# Make RIN a number and not a factor
qual$RIN.. = as.numeric(as.character(qual$RIN..))

# Make a sample ID to match those in the quality data
key$sampleid = paste(key$X,key$mouse,sep="")
key$sampleid = tolower(key$sampleid)
qual$Sample.ID = tolower(qual$Sample.ID)

# Find the corresponding samples and merge the RIN data
qual_indices = match(key$sampleid,qual$Sample.ID)
key$RIN=qual$RIN..[qual_indices]

\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Read In and Normalize}
\begin{lstlisting}[language=R]
	
# Read in the Raw data
row.names(key) = key$CEL.file.name;
affy.data = ReadAffy(phenoData = key)

# GCRMA Normalization
data.gcrma.norm=gcrma(affy.data)
gcrma=exprs(data.gcrma.norm)
boxplot(gcrma)

\end{lstlisting}
\end{frame}


\begin{frame}[fragile]{Two-Sample T-Test}
\begin{lstlisting}[language=R]
	
# Let's try a Two Sample T-Test between controls and C. neoformans
# Get the columns for all the controls
controls = key$Infection == "C-PBS"
# Get the column indices for all the C. neoformans
cneo = key$Infection == "C. neoformans"

# Perform a Two-Sample T-Test for every gene - we can use the "apply" function
pvals = apply(gcrma,1,function(x) t.test(x[controls],x[cneo],var.equal=TRUE) $p.val)

# Something weird happens when the data are "essentially constant" - in other words if the variance is 0
# We can use something called a "try-catch"
pvals = apply(gcrma,1,function(x) tryCatch(t.test(x[controls],x[cneo],var.equal=TRUE) $p.val, error=function(x) NA))
# This function sets the pvalue to NA if the data are constant
# Typically, we would actually remove genes with zero variance prior to the testing, but this is one option if you want to keep the full dimensions

# How may are significant?
sum(pvals<0.05,na.rm=TRUE)

\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Two-Sample T-Test}
\begin{lstlisting}[language=R]
	
> controls
 [1] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
[14] FALSE FALSE FALSE FALSE FALSE FALSE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE
[27]  TRUE  TRUE
> cneo
 [1]  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE  TRUE FALSE FALSE FALSE FALSE
[14] FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE FALSE
[27] FALSE FALSE
> pvals = apply(gcrma,1,function(x) t.test(x[controls],x[cneo],var.equal=TRUE) $p.val)
 Show Traceback
 
 Rerun with Debug
 Error in t.test.default(x[controls], x[cneo], var.equal = TRUE) : 
  data are essentially constant 
  # Something weird happens when the data are "essentially constant" - in other words if the variance is 0
  # We can use something called a "try-catch"
  > pvals = apply(gcrma,1,function(x) tryCatch(t.test(x[controls],x[cneo],var.equal=TRUE) $p.val, error=function(x) NA))
   
  # How may are significant?
  > sum(pvals<0.05,na.rm=TRUE)
  [1] 4683
  # Wow that's a lot! But remember, we performed over 20,000 hypothesis tests

\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Correct for Multiple Testing}
\begin{lstlisting}[language=R]
	
# Let's try a Bonferonni Correction
pvals_bf = p.adjust(pvals[!is.na(pvals)], method = "bonferroni", n = length(pvals[!is.na(pvals)]))

# How many are significant?
sum(pvals_bf<0.05,na.rm=TRUE)

# Let's try a less stringent Benjamini-Hochberg Correction
pvals_bh = p.adjust(pvals[!is.na(pvals)], method = "BH", n = length(pvals))

# How many are significant?
sum(pvals_bh<0.05,na.rm=TRUE)

# Using Benjamini-Hochberg Corrected P-Values, let's get a significant probe list
probe_list = names(pvals_bh[pvals_bh<0.05 & !is.na(pvals_bh)])

# What specific questions would you be interested in? Perhaps try an ANOVA to see if there is any difference between any infection groups. Are you only interested in brain infections? Outline your specific research questions and use the appropriate hypothesis tests to answer them.
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Correct for Multiple Testing}
\begin{lstlisting}[language=R]

# Let's try a Bonferonni Correction
> pvals_bf = p.adjust(pvals[!is.na(pvals)], method = "bonferroni", n = length(pvals[!is.na(pvals)]))
> 
# How many are significant?
> sum(pvals_bf<0.05,na.rm=TRUE)
[1] 10

# Let's try a less stringent Benjamini-Hochberg Correction
> pvals_bh = p.adjust(pvals[!is.na(pvals)], method = "BH", n = length(pvals))
 
# How many are significant?
> sum(pvals_bh<0.05,na.rm=TRUE)
[1] 644
 
# Using Benjamini-Hochberg Corrected P-Values, let's get a significant probe list
> probe_list = names(pvals_bh[pvals_bh<0.05 & !is.na(pvals_bh)])
> probe_list
  [1] "1415681_at"                "1415683_at"               
  [3] "1415691_at"                "1415692_s_at"             
  [5] "1415700_a_at"              "1415705_at"               
  [7] "1415723_at"                "1415740_at"               
  [9] "1415761_at"                "1415762_x_at"    ...
\end{lstlisting}
\end{frame}

\begin{frame}{On your own}
	\begin{itemize}
		\item What specific questions would you be interested in? 
		\item Perhaps try an ANOVA to see if there is any difference between any infection groups. 
		\item Are you only interested in brain infections? 
		\item Outline your specific research questions and use the appropriate hypothesis tests to answer them.
	\end{itemize}
\end{frame}

\begin{frame}{Next Time}
	\begin{itemize}
		\item Classification
		\item Tuesday, 3/1/16 @ 12:15pm in North 100
	\end{itemize}
\end{frame}

\end{document}
