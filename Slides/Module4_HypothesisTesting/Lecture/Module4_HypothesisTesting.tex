\documentclass[compress]{beamer}
\usetheme{Warsaw}
%\usepackage[utf8]{inputenc}
%\usepackage{default}
\setbeamertemplate{footline}[text line]{}
\useinnertheme{circles}
\usepackage{url}
\usepackage[export]{adjustbox}
\usepackage{tikz}
\usetikzlibrary{arrows,shapes,snakes,automata,backgrounds,fit,petri}
\definecolor{imp}{rgb}{0.76,0.242,0.00}
\definecolor{nimp}{RGB}{132,132,143}
\definecolor{bc1}{RGB}{26,37,57}
\definecolor{bc2}{RGB}{36,107,155}
\newcommand{\imp}[1]{{\color{imp} #1}}
\newcommand{\nimp}[1]{{\color{nimp} #1}}
\setbeamercolor*{palette primary}{use=structure,fg=white,bg=bc1}
\setbeamercolor*{palette quaternary}{fg=white,bg=bc2} 
\usepackage[english]{babel}
\usepackage[applemac]{inputenc}
\usepackage{times}
\usepackage[T1]{fontenc}

\title{ { Biomarker Discovery Workshop }}
\subtitle{ Module 4: Hypothesis Testing }
\author{\small Instructor: Ashlee Valente}
\institute{Center for Applied Genomics and Precision Medicine}
\date[ uh ]{\small February 2, 2016}

\begin{document}

\begin{frame}
	\titlepage
%	\includegraphics[height = 0.75cm,center]{images/both_logo.png}
\end{frame}


\begin{frame}{Workshop Overview}
 \centering
\includegraphics[scale=0.17]{images/StatsWorkshop.pdf}
\end{frame}

\begin{frame}{}
 \centering
Caution: Many formulas ahead \\
\vspace{2mm}
Also, thank you Wikipedia...
\end{frame}


\section{Intro to Hypothesis Tests}

\begin{frame}{What is Hypothesis Testing?}
\begin{itemize}
\item Hypothesis testing: drawing inference about a population based on statistical evidence from a sample
\item An example: Gas Price
\footnotesize
\begin{itemize}
\item Suppose someone says that the average price of a gallon of regular unleaded gas in Massachusetts today is \$1.99. 
\item To test this, we could find prices at every gas station in the state...definitive, but time-consuming and costly.
\item Simpler: Find prices at a small number of randomly selected gas stations, and then compute the sample average.
\item Your sample average comes out to be \$2.02. Is the \$0.03 difference an artifact of random sampling or significant evidence that the average price of a gallon of gas was in fact greater than \$1.99?
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Some Terminology}
\begin{itemize}
\item \textbf{Null Hypothesis}: Assertion about a population that you would like to test (typically represents a generally accepted belief, or lack of an effect) 
	\begin{itemize}
		\item $H_0: \mu = 1.99$
	\end{itemize}
\item \textbf{Alternative Hypothesis}: Contrasting assertion about the population that can be tested against the null hypothesis. 
	\begin{itemize}
		\item $H_1: \mu \neq 1.99$ (two-sided)
		\item $H_1: \mu > 1.99$ (right one-sided)
		\item $H_1: \mu < 1.99$ (left one-sided)
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Some Terminology}
\begin{itemize}
	\item \textbf{Test Statistic}: To conduct a hypothesis test, a random sample from the population is collected and a relevant test statistic is computed to summarize the sample (varies with the type of test, but its distribution under the null hypothesis must be known or assumed).
	\item \textbf{P-Value}: The probability, under the null hypothesis, of obtaining a value of the test statistic as extreme or more extreme than the value computed from the sample.
	\begin{itemize}
		\item If the true average gas price today in Massachusetts is \$1.99, our p-value for a right-tailed test is the probability that any random sample is greater than or equal to \$2.02 (the test statistic of our random sample).
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Some Terminology}
	\footnotesize
\begin{itemize}
 \item \textbf{Significance Level}: A threshold of probability agreed to before the test is conducted - denoted $\alpha$, and often 0.05. 
 \begin{itemize}
 	\footnotesize
 	\item If the p value of a test is less than $\alpha$, reject $H_0$. 
	\item If the p value is greater than $\alpha$, there is insufficient evidence to reject $H_0$.
	\item \textbf{Important:}  Note that \textbf{lack of evidence} for rejecting $H_0$ is \textbf{not evidence for accepting} $H_0$.
	\item $\alpha$ can be interpreted as the probability of rejecting $H_0$ when it is actually true. 
	\end{itemize}
	\centering
	\includegraphics[scale=0.45]{images/htest2.pdf}
\end{itemize}
\end{frame}

\begin{frame}{Some Terminology}
\begin{itemize}	
	\footnotesize
	\item \textbf{Type I Error}: Rejecting $H_0$ when it is actually true (false positive).
	\item \textbf{Type II Error}: Failing to reject $H_0$ when it is actually false (false negative).
	\item \textbf{Statistical Power}: The probability of correctly rejecting a false $H_0$ ($1-\beta$, where $\beta$ is the probability of a false negative.
\end{itemize}
	\centering
	\includegraphics[scale=0.45]{images/type1type2.png}
\end{frame}


%
 \begin{frame}{Hypothesis Test Assumptions}
\begin{itemize}	
	\item Different hypothesis tests make different assumptions about the distribution of the random variable being sampled in the data.
	\item These assumptions must be considered when choosing a test and when interpreting the results.
	\item For example, the z-test and the t-test both assume that the data are independently sampled from a normal distribution.
	\item Always be mindful of these assumptions - if they are not (reasonably) met, your results are invalid.
	\end{itemize}

 \end{frame}
 
\begin{frame}{Types of Hypothesis Tests}
\begin{itemize}	
	\footnotesize
\item \textbf{Distribution tests}, such as Anderson-Darling and one-sample Kolmogorov-Smirnov, test whether sample data comes from a population with a particular distribution. Test whether two sets of sample data have the same distribution using tests such as two-sample Kolmogorov-Smirnov.
\item \textbf{Location tests}, such as z-test and one-sample t-test, test whether sample data comes from a population with a particular mean or median. Test two or more sets of sample data for the same location value using a two-sample t-test or multiple comparison test.
\item \textbf{Dispersion tests} test whether sample data comes from a population with a particular variance.
\item \textbf{Parametric vs. Non-Parametric}: Non-parametric tests make fewer assumptions about the data and are less affected by outliers, but provide less statistical power.
\end{itemize}
 \end{frame}

\section{Common Tests}

\begin{frame}{Commonly-Used Tests}
 \begin{itemize} 
	 \item Z-Test
	 \item T-Test (One Sample)
	 \item T-Test (Two Sample)
	 \item T-Test (Paired)
	 \item Wilcoxon Rank Sum (Think non-parametric t-test)
	 \item Wilcoxon Sign Rank (Think non-parametric paired t-test)
	 \item ANOVA
	 \item Chi-Square Test
	 \item Fisher Exact Test
 \end{itemize}
 \end{frame}
 
\begin{frame}{Z-Test}
	\footnotesize
	Use it when: 
	\begin{itemize}
		\item Comparing the mean of a set of measurements to a constant
		\item Your test statistic under $H_0$ can be approximated by a normal distribution (often true with large sample sizes)
		\item The population standard deviation is known
	\end{itemize}
	\vspace{5mm}
	\LARGE
	\centering
	$z = \frac{\bar{X}-\mu}{\frac{\sigma}{\sqrt{N}}} \sim N(0,1)$
	% \includegraphics[scale=0.6]{images/zformula.png}
\end{frame}

\begin{frame}{Z-Test}
	\centering
	\includegraphics[scale=0.25]{images/ztest_simple.pdf}
\end{frame}

  
\begin{frame}{One-Sample T-Test}
	\footnotesize
	\begin{itemize}
		\item Student's t-distribution (or simply the t-distribution) is any member of a family of continuous probability distributions that arises when estimating the mean of a normally distributed population in situations where the sample size is small and population standard deviation is unknown. 
\item Use it when: 
	\begin{itemize}
		\footnotesize
		\item Comparing the mean of a set of measurements to a constant
		\item Your test statistic under $H_0$ can be approximated by a normal distribution (often true with large sample sizes)
		\item The population standard deviation is \textbf{not} known
	\end{itemize}
	\end{itemize}
	\vspace{5mm}
	\LARGE
	\centering
	$t = \frac{\bar{X}-\mu}{\frac{s}{\sqrt{N}}}$
	
\end{frame}	

\begin{frame}{Two-Sample T-Test}
	\footnotesize
	\begin{itemize}
		\item Use it when:
	\begin{itemize}
		\footnotesize
		\item Testing whether the means of two \textbf{independent} groups differ
		\item The population standard deviation is \textbf{not} known
		\item There are different formulas depending on whether the variances and sample sizes are the same
	\end{itemize}
	\end{itemize}
	\vspace{5mm}
	\LARGE
	\centering
	$t = \frac{\bar{X_1}-\bar{X_2}}{ \sqrt{s^2_{X_1} + s^2_{X_2}}\sqrt{\frac{1}{N}}}$
\end{frame}

\begin{frame}{Paired T-Test}
	\footnotesize
	\begin{itemize}
		\item Use it when:
	\begin{itemize}
		\footnotesize
		\item Testing whether the means of two \textbf{dependent} groups differ (repeated measures or matched samples)
		\item The population standard deviation is \textbf{not} known
		\item There are different formulas depending whether the variances are the same
	\end{itemize}
	\end{itemize}
	\vspace{5mm}
	\LARGE
	\centering
	$t = \frac{\bar{X_D}-\mu_0}{\frac{s_D}{\sqrt{N}}}$
\end{frame}

\begin{frame}{Wilcoxon Rank Sum (Mann-Whitney U-test)}
	\begin{itemize}
		\footnotesize
		\item Use it when:
		\begin{itemize}
			\footnotesize
			\item Testing whether the medians of two \textbf{independent} groups differ
			\item The distributional assumptions of a T-test are not met
		\end{itemize}
	\item Assign numeric ranks to all the observations, beginning with 1 for the smallest value.
	\item Where there are groups of tied values, assign a rank equal to the midpoint of unadjusted rankings (e.g., the ranks of (3, 5, 5, 9) are (1, 2.5, 2.5, 4)).
	\item Add up the ranks for the observations which came from group 1.
	\item $U_1 = R_1 - \frac{n_1(n_1+1)}{2}$, similar for group 2.
	\item where $n_1$ is the sample size for group 1, and $R_1$ is the sum of the ranks in group 1
	\item For large samples, U is approximately normally distributed. In that case, the standardized value
	\item $z = \frac{ U - m_U }{ \sigma_U }$
	\item $m_U = \frac{n_1 n_2}{2}$,  \,  and
	\item $\sigma_U=\sqrt{\frac{n_1 n_2 (n_1 + n_2+1)}{ 12}}$
\end{itemize}

\end{frame}

\begin{frame}{Wilcoxon Sign Rank}
	\begin{itemize}
		\item Use it when:
		\begin{itemize}
		\item Testing whether the medians of two \textbf{dependent} groups differ (repeated measures or matched samples)
		\item The distributional assumptions of a T-test are not met
		\end{itemize}
		\item Similar concept as the Wilcoxon Rank Sum, but a bit more complex
		\end{itemize}
		
\end{frame}

\begin{frame}{ANOVA}
	\begin{itemize}
		\item A collection of statistical models used to analyze the differences among group means.
		\item In its simplest form, ANOVA provides a statistical test of whether or not the means of several groups are equal, and therefore generalizes the t-test to more than two groups.
		\item Use it when:
		\begin{itemize}
		\item Testing whether the means of multiple \textbf{independent} groups differ
		\item The distributions of the residuals (noise) are normal
		\item The variance of data within groups is the same (homoscedasticity)
		\end{itemize}
	\end{itemize}
		
\end{frame}

\begin{frame}{ANOVA}
		\begin{itemize}
		\item ANOVA partitions the total sum of squares (SST) into sum of squares due to between-groups effect (SSR) and sum of squared errors (SSE).
		\item SSR = Variation of group means from the overall mean
		\item SSE = Variation of observations in each group from their group mean estimates
		\end{itemize}
	\centering
	\includegraphics[scale=0.25]{images/anova1.png} \\
	\includegraphics[scale=0.25]{images/anova2.png}

\end{frame}


\begin{frame}{Contingency Tables}
	\footnotesize
	
	\begin{itemize}
		\item A contingency table is a type of table in a matrix format that displays the (multivariate) frequency distribution of variables.
		\item Example: Sex differences in handedness
		\end{itemize}
		\centering
		\footnotesize
\begin{tabular}{ c | c | c | c }
  \textbf{Gender} & \textbf{Right-handed} & \textbf{Left-handed} & \textbf{Total}\\
  \hline
\textbf{Male} & 43 & 9 & 52 \\
\textbf{Female} & 44 & 4 & 48 \\
\textbf{Total} & 87 & 13 & 100 \\
\end{tabular}
	\begin{itemize}
		\item The numbers of the males, females, and right- and left-handed individuals are called marginal totals.
		\item The table allows us to see at a glance that the proportion of men who are right-handed is about the same as the proportion of women who are right-handed although the proportions are not identical.
		\item The significance of the difference between the two proportions can be assessed with a variety of statistical tests.
		\item If the proportions of individuals in the different columns vary significantly between rows (or vice versa), the two variables are not independent.
		\item A confusion matrix is a type of contingency table.
		\end{itemize}
\end{frame}

\begin{frame}{Chi Square Test}
	\begin{itemize}
		\footnotesize
		\item Chi-squared ($\chi^2$) tests are often constructed from a sum of squared errors, and assume independent normally distributed data.
		\item Use it when:
			\begin{itemize}
				\footnotesize
				\item You wish to determine whether there is a significant difference between the expected frequencies and the observed frequencies in one or more categories. (Does the number of individuals or objects that fall in each category differ significantly from the number you would expect?)
			\end{itemize}
		\item $\chi^2 = \sum_{i=1}^{n} \frac{(O_i - E_i)^2}{E_i} =  N \sum_{i=1}^n p_i \left(\frac{O_i/N - p_i}{p_i}\right)^2 $
		\item $\chi^2$ = Pearson's cumulative test statistic, which asymptotically approaches a $\chi^2$ distribution.
	\item $O_i$ = the number of observations of type $i$
	\item $N$ = total number of observations
	\item $E_i$ = the expected (theoretical) frequency of type $i$
	\item $n$ = the number of cells in the table
	\item The chi-squared statistic can then be used to calculate a p-value by comparing the value of the statistic to a chi-squared distribution.
	\end{itemize}
\end{frame}

\begin{frame}{Chi Square Test}
	\begin{itemize}
		\footnotesize
	\item Example: Fairness of Dice
	\item A 6-sided die is thrown 60 times:
		\end{itemize}
		\footnotesize
		\centering
			\begin{tabular}{ c | c | c | c | c | c }
				\footnotesize
				
			$i$	& $O_i$ & $E_i$ & $O_i - E_i$ & $(O_i - E_i)^2$ & $\frac{(O_i-E_i)^2}{E_i}$ \\
			\hline
			\hline
			1 & 5 & 10 & -5 & 25 & 2.5 \\
			2 & 8 & 10 & -2 & 4 & 0.4 \\
			3 & 9 & 10 & -1 & 1 & 0.1 \\
			4 & 8 & 10 & -2 & 4 & 0.4 \\
			5 & 10 & 10 & 0 & 0 & 0 \\
			6 & 20 & 10 & 10 & 100 & 10 \\
			\hline
			\hline
			 &  &  &  & \textbf{Sum} & \textbf{13.4} \\
			 \end{tabular}

			\begin{itemize}
				\item The number of degrees of freedom is n − 1 = 5.
				\item The Upper-tail critical values of chi-square distribution table gives a critical value of 11.070 at 95\% significance level
				\item As the chi-squared statistic of 13.4 exceeds this critical value, we reject the null hypothesis and conclude that the die is biased at 95\% significance level.

	\end{itemize}
\end{frame}

\begin{frame}{Fisher's Exact Test}
	\begin{itemize}
		\item Fisher's exact test is a statistical significance test used in the analysis of contingency tables.
		\item Use it when:
			\begin{itemize}
				\item You wish to determine whether there is a significant difference between the expected frequencies and the observed frequencies in one or more categories.
				\item Often used when sample sizes are small.
				\item With large samples, a $\chi^2$ test can be used in this situation. However, the significance value it provides is only an approximation.%, because the sampling distribution of the test statistic that is calculated is only approximately equal to the theoretical chi-squared distribution.
			\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{Fisher's Exact Test}
	\begin{itemize}
		\footnotesize
		\item Example: Teenage Dieters
		\item We hypothesize, for example, that the proportion of dieting individuals is higher among the women than among the men, and we want to test whether any difference of proportions that we observe is significant.
		\end{itemize}
	\footnotesize
			
\begin{tabular}{ c | c | c | c }
	    & Men & Women & Row total \\
	   \hline
		Dieting & 1 & 9 & 10 \\
	  Non-dieting & 11 & 3 & 14 \\
   \hline
	Column total & 12 & 12 & 24 \\
	\end{tabular}
\end{frame}

\begin{frame}{Fisher's Exact Test}
	\begin{itemize}
		\footnotesize
		\item Null hypothesis: men and women are equally likely to diet
		\item What is the probability that these 10 dieters would be so unevenly distributed between the women and the men?
		\end{itemize}
		\footnotesize
		\vspace{2mm}
		\begin{tabular}{ c | c | c | c }
	    & Men & Women & Row total \\
	   \hline
		Dieting & a & b & a + b \\
	  Non-dieting & c & d & c + d \\
   \hline
	Column total & a + c & b + d & a + b + c + d (=n) \\
	\end{tabular}
	\vspace{2mm}
	
	\begin{itemize}
		\footnotesize
			\item Fisher showed that the probability of obtaining any such set of values was given by the hypergeometric distribution:
			\item $p = \frac{ \displaystyle{{a+b}\choose{a}} \displaystyle{{c+d}\choose{c}} }{ \displaystyle{{n}\choose{a+c}} } = \frac{(a+b)!~(c+d)!~(a+c)!~(b+d)!}{a!~~b!~~c!~~d!~~n!}$
			% \item $p = { {\tbinom{10}{1}} {\tbinom{14}{11}} }/{ {\tbinom{24}{12}} } = \tfrac{10!~14!~12!~12!}{1!~9!~11!~3!~24!} \approx 0.001346076$
			\item The formula above gives the exact hypergeometric probability of observing this particular arrangement of the data, assuming the given marginal totals, on the null hypothesis that men and women are equally likely to be dieters.
			\end{itemize}
\end{frame}


\section{Multiple Testing}

\begin{frame}{Multiple Testing}

	\begin{itemize}
		\footnotesize
		\item Errors in inference (e.g. hypothesis tests that incorrectly reject the null hypothesis), are more likely to occur when one considers a large number of related tests.
		\item Several statistical techniques have been developed to alleviate these errors.
		\item If one test is performed at the 5\% level, there is only a 5\% chance of incorrectly rejecting the null hypothesis if the null hypothesis is true. However, for 100 tests where all null hypotheses are true, the expected number of incorrect rejections is 5.
		\item Family Wise Error Rate (FWER) vs. False Discovery Rate (FDR) - 
		\begin{itemize}
			\footnotesize
			\item FWER control exerts a more stringent control over false discovery compared to FDR procedures. 			\item FWER control limits the probability of at least one false discovery.
			\item FDR control limits the expected proportion of false discoveries.
			\end{itemize}
		\item "Family" - the smallest set of items of inference in an analysis, interchangeable about their meaning for the goal of research
		\end{itemize}
 \end{frame}

\begin{frame}{Bonferroni (FWER) and Benjamini-Hochberg (FDR)}
	\centering
	\includegraphics[scale=0.3]{images/MHCorrection.pdf}

\end{frame}

\begin{frame}{Next Time}
\begin{itemize}
\item Tuesday, February 16th at 12:15, North Building 100
\item Code will available on Basecamp prior to the practical session
\end{itemize}
\end{frame}


\end{document}
