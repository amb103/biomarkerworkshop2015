\beamer@endinputifotherversion {3.36pt}
\select@language {english}
\beamer@sectionintoc {1}{Read In Data}{3}{0}{1}
\beamer@sectionintoc {2}{Proc. and Normalization}{12}{0}{2}
\beamer@sectionintoc {3}{PVCA}{16}{0}{3}
\beamer@sectionintoc {4}{Batch Effects}{18}{0}{4}
