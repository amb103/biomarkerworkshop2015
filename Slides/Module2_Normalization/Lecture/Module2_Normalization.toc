\beamer@endinputifotherversion {3.36pt}
\select@language {english}
\beamer@sectionintoc {1}{Pre-Processing and Normalization}{3}{0}{1}
\beamer@sectionintoc {2}{PCA}{16}{0}{2}
\beamer@sectionintoc {3}{Batch Effects}{18}{0}{3}
