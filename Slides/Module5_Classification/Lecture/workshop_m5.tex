%!TEX TS-program = pdflatex
\documentclass[compress,9pt]{beamer}
%
\usetheme{default}
\useinnertheme{circles}
\usefonttheme{serif}
\usefonttheme{structuresmallcapsserif}
\usecolortheme{seagull}
\useoutertheme[subsection=false]{miniframes}
\setbeamertemplate{mini frames}[tick]
\setbeamertemplate{footline}
{
  \leavevmode%
  \hbox{%
  \begin{beamercolorbox}[wd=.9\paperwidth,ht=2.2ex,dp=1ex,left]{author in head/foot}%
    \usebeamerfont{author in head/foot}\hspace*{1ex}CAGPM
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.1\paperwidth,ht=2.2ex,dp=1ex,right]{title in head/foot}%
    \usebeamerfont{title in head/foot}
    \insertframenumber{} / \inserttotalframenumber\hspace*{1ex}
  \end{beamercolorbox}}%
  \vskip0pt%
}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{frametitle}[default][left]
\setbeamersize{text margin left=2em,text margin right=2em,mini frame size=2pt}
\setbeamerfont{headline}{size=\tiny}
\setbeamertemplate{sections/subsections in toc}[default]
%
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{bm}
\usepackage{url}
\usepackage{graphicx}
% \usepackage{subfigure}
\usepackage{psfrag}
\usepackage{url}
% \usepackage[absolute]{textpos}
% \usepackage[overlay]{textpos}
% \usepackage{textpos}
% \setlength{\TPHorizModule}{30mm}
% \setlength{\TPVertModule}{\TPHorizModule}
% \textblockorigin{10mm}{10mm}
\usepackage{tikz}
\usetikzlibrary{arrows,shapes,snakes,automata,backgrounds,fit,petri}
\usepackage{rotating}
\usepackage{adjustbox}

%
% Definitions
\input{/Users/rhenao/Tex/bib_files/miscdefs.tex}
%
\definecolor{imp}{rgb}{0.76,0.22,0.00}
\definecolor{nimp}{rgb}{0.00,0.66,0.32}
\definecolor{bc1}{RGB}{26,37,57}
\definecolor{bc2}{RGB}{36,107,155}
\newcommand{\imp}[1]{{\color{imp} #1}}
\newcommand{\nimp}[1]{{\color{nimp} #1}}
%
\setbeamercolor*{palette primary}{use=structure,fg=white,bg=bc1}
\setbeamercolor*{palette quaternary}{fg=white,bg=bc2} 
%
% \newcommand{\gargantuan}{\fontsize{4}{4}\selectfont}
% 
% \makeatletter
% \renewcommand{\@thesubfigure}{\tiny{\thesubfigure}\space}
% \makeatother
%
% \usepackage[english]{babel}
\usepackage[applemac]{inputenc}
\usefonttheme{serif}
\usepackage[T1]{fontenc}
%
\makeatletter
\newcommand{\srcsize}{\@setfontsize{\srcsize}{5pt}{5pt}}
\makeatother
%
\title{ {\sc Biomarker Discovery Workshop } }
%
\subtitle{ Module 5: Classification }
%
\author{ }
%
\institute{ Center for Applied Genomics and Precision Medicine }
%
\date[ uh ]{March 15, 2016}
%
% \pgfdeclareimage[height = 0.5cm]{university-logo}{university-logo-filename}
% \logo{\pgfuseimage{university-logo}}
%
\begin{document}
%
\begin{frame}
	\titlepage
\end{frame}
%
\begin{frame}[c]\frametitle{Workshop Overview}
	\centering
	\includegraphics[scale=0.17]{./images/statsworkshop.pdf}
\end{frame}
%
\section{Motivation}
%
\begin{frame}[c]\frametitle{Motivation}
	Goal: estimate relationship between targets ($y$) and attributes ($\x$): $y = f(\x)$
	
	\vspace{4mm}
	Considerations:
	\begin{itemize}
		\item Domain of $y$.
		\item Domain of $\x$, how large is $\x$ (number of elements)?
		\item Form of $f(\cdot)$, {\bf parametric}/nonparametric, {\bf linear}/nonlinear, \emph{etc.}
		\item Objective of the estimation procedure.
	\end{itemize}
	
	\vspace{4mm}
	Of particular interest (classification):
	
	\begin{itemize}
		\item $y\in\{0,1\}$, binary label, \emph{e.g.}, control/case, healthy/sick, \emph{etc.}
		\item $\x\in\Re^d$, $d$-dimensional vector of continuous variables, \emph{e.g.}, gene expression.
		\item $y$ is a linear function of $\x$.
		\item Minimize prediction error.
		\item Generalization ability.
	\end{itemize}
\end{frame}
%
\section{Linear Regression}
%
\begin{frame}[c]\frametitle{Example: curve fit}
	\begin{itemize}
		\item Regression problem.
		\item Predict real-valued $t$ based on observed real-valued $x$.
		\item We have a training set composed of $N=10$ pairs $\{t_n,x_n\}$, for $n=1,\ldots,N$.
		\item Supervised setting.
	\end{itemize}
	
	\vspace{2mm}
	Ground truth: $t = \sin(2\pi x)$, however we observe a noisy version of it.
	
	\vspace{2mm}
	Goal: use $\{t_n,x_n\}$ to uncover the ground truth, but use finite and noisy data.
	
	\vspace{6mm}
	\centering
	\includegraphics[scale=0.8]{./images/Figure12}
\end{frame}
%
\begin{frame}[c]\frametitle{Example: curve fit}
	Lets use the model (polynomial function):
	%
	\begin{align}
		y(x,\w) = w_0+w_1x+w_2x^2+\ldots+w_Mx^M
	\end{align}
	
	\vspace{2mm}
	\begin{itemize}
		\item $\w$ is the set of (unknown) parameters, $w_1,\ldots,w_M$.
		\item $y(x,\w)$ is a linear function of $\w$.
		\item $y(x,\w)$ is a nonlinear function of $x$.
		\item $y(x,\w)$ is a linear model.
	\end{itemize}
	
	\vspace{2mm}
	Goal: minimize error, misfit between $y(x_n,\w)$ and $t_n$
	%
	\begin{align}
		E(\w) = \sum_{n=1}^N \{y(x_n,\w)-t_n\}^2
	\end{align}
	
	\vspace{2mm}
	\centering
	\includegraphics[scale=0.5]{./images/Figure13}
\end{frame}
%
\begin{frame}[c]\frametitle{Example: curve fit}
	\begin{itemize}
		\item Estimating $\w$ is easy because $y(x,\w)$ is linear.
		\item What about $M$?
	\end{itemize}
	
	\vspace{2mm}
	\centering
	\includegraphics[scale=0.55]{./images/Figure14a}
	\includegraphics[scale=0.55]{./images/Figure14b}\\
	\includegraphics[scale=0.55]{./images/Figure14c}
	\includegraphics[scale=0.55]{./images/Figure14d}
\end{frame}
%
\begin{frame}[c]\frametitle{Example: curve fit}
	\begin{itemize}
		\item Lets consider a test set of 100 points.
		\item Compare training and testing errors.
		\item Use root-mean-square (RMS) error: $\sqrt{E(\w)/N}$.
	\end{itemize}
	
	\vspace{2mm}
	\centering
	\includegraphics[scale=0.8]{./images/Figure15}
\end{frame}
%
\begin{frame}[c]\frametitle{Example: curve fit}
	Lets examine the parameters of the model ($\w$):
	
	\vspace{4mm}
	\begin{center}
		\includegraphics[scale=0.8]{./images/FigureW}
	\end{center}
	
	\vspace{4mm}
	As $E(\w)\to 0$, we are fitting the noise in addition to the ground truth.
\end{frame}
%
\begin{frame}[c]\frametitle{Example: curve fit}
	What happens if we increase $N=\{10,15,100\}$ but fix $M=9$?
	
	\vspace{4mm}
	\begin{center}
		\includegraphics[scale=0.5]{./images/Figure14d}
		\includegraphics[scale=0.5]{./images/Figure16a}
		\includegraphics[scale=0.5]{./images/Figure16b}
	\end{center}
	
	\vspace{4mm}
	The larger the data set, the more complex (flexible) the model we can afford.
\end{frame}
%
\begin{frame}[c]\frametitle{Example: curve fit}
	How can we choose the complexity of the problem according to the complexity of the problem?
	
	\begin{itemize}
		\item Option 1 (selection): control the number of parameters.
		\item Option 2 (regularization): control the behavior of the parameters.
		\item Option 3 (next session): do both.
	\end{itemize}
	
	\vspace{4mm}
	Option 2: discourage coefficients from reaching large values
	%
	\begin{align}
		\tilde{E}(\w) = \sum_{n=1}^N \{y(x_n,\w)-t_n\}^2 + \lambda \sum_{m=1}^M w_m^2
	\end{align}
	
	\vspace{4mm}
	\begin{center}
		\includegraphics[scale=0.6]{./images/Figure17a}
		\includegraphics[scale=0.6]{./images/Figure17b}
	\end{center}
\end{frame}
%
\begin{frame}[c]\frametitle{Example: curve fit}
	Lets check the estimated parameters and RMS error again:
	
	\vspace{4mm}
	\begin{center}
		\includegraphics[scale=0.76]{./images/FigureWlambda}
		\includegraphics[scale=0.6]{./images/Figure18}
	\end{center}
	
	\vspace{4mm}
	\begin{itemize}
		\item $\lambda$ controls complexity and determines overfitting.
		\item How can we set $\lambda$? (validation or cross-validation, next session).
	\end{itemize}
\end{frame}
%
\begin{frame}[c]\frametitle{Example: curve fit}
	Take home messages (general to supervised models):
	
	\vspace{4mm}
	\begin{itemize}
		\item We want a model that estimates the ground truth (without the noise).
		\item We estimate the model using supervision (training set).
		\item Too complex models may cause overfitting.
		\item Test sets help diagnose overfitting.
		\item Model size, data set size and overfitting are interrelated.
		\item Overfitting can be addressed in a principled way.
	\end{itemize}
\end{frame}
%
\section{Linear Classification}
%
\begin{frame}[c]\frametitle{Linear classification}
	Assume we have a two-class problem (cases and controls, ${\cal C}_1$ and ${\cal C}_2$, respectively), we want to estimate
	%
	\begin{align}
		p({\cal C}_1|\x) =\frac{p(\x|{\cal C}_1)p({\cal C}_1)}{p(\x|{\cal C}_1)p({\cal C}_1)+p(\x|{\cal C}_2)p({\cal C}_2)}
	\end{align}
	%
	\begin{itemize}
		\item $p({\cal C}_1)$ is the prior probability for cases.
		\item $p(\x|{\cal C}_1)$ is the likelihood for cases.
		\item $p({\cal C}_1|\x)$ is the posterior for cases.
	\end{itemize}
	
	\vspace{4mm}
	Goal: minimize the risk of assigning $\x$ to the incorrect class.
	
	\vspace{4mm}
	\centering
	\includegraphics[scale=0.6]{./images/Figure124}
\end{frame}
%
\begin{frame}[c]\frametitle{Linear classification}
	Classification as a \emph{discriminative model} is a two-stage problem:
	%
	\begin{itemize}
		\item Inference: estimate $p({\cal C}_1|\x)$.
		\item Prediction or decision: use $p({\cal C}_1|\x)$ to make an \quotes{optimal} assignment.
	\end{itemize}
	
	\vspace{4mm}
	Alternatively: Find a function $f(\x)$ that maps $\x$ to the class label.
	
	\vspace{4mm}
	Lets consider a generalization of the linear regression model
	%
	\begin{align}
		y(\x) = f\left( w_0 + \sum_{m=1}^M w_m x_m \right)
	\end{align}
	%
	\begin{itemize}
		\item We have $M$ covariates (inputs, \emph{e.g.}, genes).
		\item $f(\cdot)$ is a nonlinear function (activation or link).
		\item $w_0$ is a bias term.
	\end{itemize}	
\end{frame}
%
\begin{frame}[c]\frametitle{Linear classification}
	Simplest case, two-clases
	%
	\begin{itemize}
		\item Let $f(\cdot)$ to be the identity, $y(\x) = w_0 + \sum_{m=1}^M w_m x_m$.
		\item Assign $\x$ to ${\cal C}_1$ if $y(\x)\geq0$.
		\item Every point such that $y(\x)=0$ defines the decision boundary.
	\end{itemize}
	
	\vspace{4mm}
	\begin{center}
		\includegraphics[scale=0.6]{./images/Figure44a}
		\includegraphics[scale=0.6]{./images/Figure44b}
	\end{center}
\end{frame}
%
\begin{frame}[c]\frametitle{Linear classification}
	Lets write again
	%
	\begin{align}
		p({\cal C}_1|\x) = & \ \frac{p(\x|{\cal C}_1)p({\cal C}_1)}{p(\x|{\cal C}_1)p({\cal C}_1)+p(\x|{\cal C}_2)p({\cal C}_2)} \\
		= & \ \frac{1}{1+\exp(-a)} = \sigma(a)
	\end{align}	
	%
	where
	%
	\begin{align}
		a=\log\frac{p(\x|{\cal C}_1)p({\cal C}_1)}{p(\x|{\cal C}_2)p({\cal C}_2)}
	\end{align}
	%
	and $\sigma(x)$ is the logistic link function (sigmoid function).
	
	\vspace{4mm}
	\begin{center}
		\includegraphics[scale=0.6]{./images/Figure49}
	\end{center}
\end{frame}
%
\begin{frame}[c]\frametitle{Linear classification}
	Logistic link function
	%
	\begin{align}
		\sigma(a) = \frac{1}{1+\exp(-a)}
	\end{align}	
	%
	\begin{itemize}
		\item Symmetry: $\sigma(-a)=1-\sigma(a)$.
		\item Log odds: $a=\log\left(\frac{\sigma(a)}{1-\sigma(a)}\right)$.
	\end{itemize}
	
	\vspace{4mm}
	Logistic regression
	%
	\begin{align}
		p({\cal C}_1|\x) = \sigma\left(w_0+\sum_{m=1}^M w_m x_m\right)
	\end{align}
	%
	\begin{itemize}
		\item $p({\cal C}_2|\x) = 1 - p({\cal C}_1|\x)$.
		\item In absence of data, $w_0=\log\left(\frac{p({\cal C}_1)}{p({\cal C}_2)}\right)$.
	\end{itemize}
\end{frame}
%
\section{Next}
%
\begin{frame}[c]\frametitle{Linear classification}
	Figures for the slides taken from:
	
	\vspace{4mm}
	Christopher M. Bishop. \emph{Pattern Recognition for Machine Learning}. Springer, 2006.
	
	\vspace{4mm}
	Figures available at:
	
	\vspace{4mm}
	\url{research.microsoft.com/en-us/um/people/cmbishop/prml/webfigs.htm}
\end{frame}
%
\begin{frame}[c]\frametitle{Next Time}
	Practical session:
	\begin{itemize}
		\item Tuesday, April 5th at 12:15, North Building 100
		\item Linear regression
		\item Logistic regression
	\end{itemize}
	
	\vspace{4mm}
	Lecture session:
	\begin{itemize}
		\item Regularized logistic regression
		\item $M>N$
		\item Variable selection
		\item Validation and cross-validation
	\end{itemize}
\end{frame}
%
% \footnotesize{Images from Bishop, Springer 2006.}
%
\end{document}
