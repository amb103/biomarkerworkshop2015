%!TEX TS-program = pdflatex
\documentclass[compress,9pt]{beamer}
%
\usetheme{default}
\useinnertheme{circles}
\usefonttheme{serif}
\usefonttheme{structuresmallcapsserif}
\usecolortheme{seagull}
\useoutertheme[subsection=false]{miniframes}
\setbeamertemplate{mini frames}[tick]
\setbeamertemplate{footline}
{
  \leavevmode%
  \hbox{%
  \begin{beamercolorbox}[wd=.9\paperwidth,ht=2.2ex,dp=1ex,left]{author in head/foot}%
    \usebeamerfont{author in head/foot}\hspace*{1ex}CAGPM
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.1\paperwidth,ht=2.2ex,dp=1ex,right]{title in head/foot}%
    \usebeamerfont{title in head/foot}
    \insertframenumber{} / \inserttotalframenumber\hspace*{1ex}
  \end{beamercolorbox}}%
  \vskip0pt%
}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{frametitle}[default][left]
\setbeamersize{text margin left=2em,text margin right=2em,mini frame size=2pt}
\setbeamerfont{headline}{size=\tiny}
\setbeamertemplate{sections/subsections in toc}[default]
%
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{bm}
\usepackage{url}
\usepackage{graphicx}
% \usepackage{subfigure}
\usepackage{psfrag}
\usepackage{url}
% \usepackage[absolute]{textpos}
% \usepackage[overlay]{textpos}
% \usepackage{textpos}
% \setlength{\TPHorizModule}{30mm}
% \setlength{\TPVertModule}{\TPHorizModule}
% \textblockorigin{10mm}{10mm}
\usepackage{tikz}
\usetikzlibrary{arrows,shapes,snakes,automata,backgrounds,fit,petri}
\usepackage{rotating}
\usepackage{adjustbox}

%
% Definitions
\input{/Users/rhenao/Tex/bib_files/miscdefs.tex}
%
\definecolor{imp}{rgb}{0.76,0.22,0.00}
\definecolor{nimp}{rgb}{0.00,0.66,0.32}
\definecolor{bc1}{RGB}{26,37,57}
\definecolor{bc2}{RGB}{36,107,155}
\newcommand{\imp}[1]{{\color{imp} #1}}
\newcommand{\nimp}[1]{{\color{nimp} #1}}
%
\setbeamercolor*{palette primary}{use=structure,fg=white,bg=bc1}
\setbeamercolor*{palette quaternary}{fg=white,bg=bc2} 
%
% \newcommand{\gargantuan}{\fontsize{4}{4}\selectfont}
% 
% \makeatletter
% \renewcommand{\@thesubfigure}{\tiny{\thesubfigure}\space}
% \makeatother
%
% \usepackage[english]{babel}
\usepackage[applemac]{inputenc}
\usefonttheme{serif}
\usepackage[T1]{fontenc}
%
\makeatletter
\newcommand{\srcsize}{\@setfontsize{\srcsize}{5pt}{5pt}}
\makeatother
%
\title{ {\sc Biomarker Discovery Workshop } }
%
\subtitle{ Module 3: Exploratory Data Analysis }
%
\author{ }
%
\institute{ Center for Applied Genomics and Precision Medicine }
%
\date[ uh ]{January 5, 2016}
%
% \pgfdeclareimage[height = 0.5cm]{university-logo}{university-logo-filename}
% \logo{\pgfuseimage{university-logo}}
%
\begin{document}
%
\begin{frame}
	\titlepage
\end{frame}
%
\begin{frame}[c]\frametitle{Workshop Overview}
	\centering
	\includegraphics[scale=0.17]{./images/statsworkshop.pdf}
\end{frame}
%
\section{Motivation}
%
\begin{frame}[c]\frametitle{Motivation}
	Getting to know your data:
	
	\begin{itemize}
		\item Basic integrity checks:
		\begin{itemize}
			\item Match data to key.
			\item Match data to annotation.
		\end{itemize}
		\item Summary statistics:
		\begin{itemize}
			\item Sizes: observations, samples, subjects, variables/covariates.
			\item Proportions: cases vs. controls, batches, sex, time points, etc.
			\item Ranges: QC thresholds, limits of detection, out of range values, outliers, etc.
			\item Distributions: age, QC metrics, zeros, missing values, etc.
		\end{itemize}
	\end{itemize}
	
	\vspace{4mm}
	Does the data look good?
	
	\begin{itemize}
		\item Are the key and annotation sufficient?
		\item Are there any obvious confounders?
		\item Are proportions, ranges and distributions reasonable?
		\item Should we drop some observations or variables?
		\item Is imputation necessary?
	\end{itemize}
\end{frame}
%
\begin{frame}[c]\frametitle{Examples}
	\begin{columns}[onlytextwidth]
		\begin{column}{0.5\textwidth}
			\centering
			\includegraphics[scale=0.2]{./images/ari_age_hist.pdf}\\
			\includegraphics[scale=0.2]{./images/ari_age_kd.pdf}
		\end{column}
		\begin{column}{0.5\textwidth}
			\centering\footnotesize
			
			Sex:
			
			\vspace{3mm}
			\begin{tabular}{c|cc}
				Value & Count & Percent \\
				\hline
				F & 151 & 55.31 \\
				M & 122 & 44.69
			\end{tabular}
			
			\vspace{6mm}
			Race:
			
			\vspace{3mm}
			\begin{tabular}{c|cc}
				Value & Count & Percent \\
				\hline
				W & 116 & 42.49 \\
				B & 135 & 49.45 \\
				U & 13 & 4.76 \\
				A & 9 & 3.30
			\end{tabular}
			
			\vspace{6mm}
			Race vs Phenotype:
			
			\vspace{3mm}
			\begin{tabular}{c|ccc}
				R$\backslash$P & P1 & P2 & P3 \\
				\hline
				W & 35 & 22 & 59 \\
				B & 49 & 46 & 40 \\
				U & 4 & 2 & 7 \\
				A & 0 & 0 & 9
			\end{tabular}
		\end{column}
	​\end{columns}
\end{frame}
%
\section{Exploration}
%
\begin{frame}[c]\frametitle{Exploration}
	Taking a deeper look at the (molecular) data:
	
	\begin{itemize}
		\item Small: boxplots or histograms if possible.
		\item Large: means, medians, variances, IQRs, etc.
	\end{itemize}
	
	\centering
	\includegraphics[scale=0.2,trim={1cm 3cm 1cm 3cm},clip]{./images/ari_xpr_mean.pdf}
	\includegraphics[scale=0.2,trim={1cm 3cm 1cm 3cm},clip]{./images/ari_xpr_median.pdf}\\
	\includegraphics[scale=0.2,trim={1cm 3cm 1cm 3cm},clip]{./images/ari_xpr_var.pdf}
	\includegraphics[scale=0.2,trim={1cm 3cm 1cm 3cm},clip]{./images/ari_xpr_iqr.pdf}
\end{frame}
%
\begin{frame}[c]\frametitle{Exploration}
	Taking a deeper look at the (molecular) data:
	
	\begin{itemize}
		\item Small: boxplots or histograms if possible.
		\item Large: means, medians, variances, IQRs, etc.
	\end{itemize}
	
	\centering
	\includegraphics[scale=0.25,trim={5cm 3cm 3cm 3cm},clip]{./images/ari_xpr_mm.pdf}
	\includegraphics[scale=0.25,trim={1cm 3cm 1cm 3cm},clip]{./images/ari_xpr_mv.pdf}
\end{frame}
%
\section{PCA}
%
\begin{frame}[c]\frametitle{Principal Component Analysis (PCA)}
	Motivation: displaced and rotated digits.
	
	\vspace{2mm}
	\begin{center}
		\includegraphics[scale=0.6]{./images/figure121.pdf}
	\end{center}
	
	\vspace{4mm}
	PCA: find a low dimensional space that maximizes variance of projected data
	
	\vspace{2mm}
	\begin{center}
		\includegraphics[scale=0.5]{./images/figure122.pdf}
	\end{center}
	
	\vspace{6mm}
	\footnotesize{Images from Bishop, Springer 2006.}
\end{frame}
%
\begin{frame}[c]\frametitle{Principal Component Analysis (PCA)}
	PCA views:
	
	\begin{itemize}
		\item Maximum variance formulation.
		\begin{center}
			\includegraphics[scale=0.6]{./images/figure126a.pdf}
			\includegraphics[scale=0.6]{./images/figure126b.pdf}
			\includegraphics[scale=0.6]{./images/figure126c.pdf}
		\end{center}
		\item Minimum error formulation.
		\begin{center}
			\includegraphics[scale=0.6]{./images/figure125.pdf}
		\end{center}
	\end{itemize}
\end{frame}
%
\section{Clustering}
%
\begin{frame}[c]\frametitle{$K$-Means}
	$K$-means: find cluster assignments for a given number of clusters, $K$.
	
	\vspace{2mm}
	\begin{center}
		\includegraphics[scale=0.7]{./images/figure91a.pdf}
		\includegraphics[scale=0.7]{./images/figure91i.pdf}
	\end{center}
	
	\vspace{18mm}
	\footnotesize{Images from Bishop, Springer 2006.}
\end{frame}
%
\begin{frame}[c]\frametitle{Hierarchical clustering}
	Hierarchical clustering: build binary tree from observations.
	
	\begin{itemize}
		\item Strategy: agglomerative or divisive.
		\item Metric: measure of similarity, e.g., euclidean, correlation, etc.
		\item Linkage: distance between clusters, e.g., minimum, average, etc.
	\end{itemize}
	
	\vspace{2mm}
	\begin{center}
		\includegraphics[scale=0.35]{./images/rppa_fm_hc}
	\end{center}
\end{frame}
%
\begin{frame}[c]\frametitle{Next Time}
	\begin{itemize}
		\item Tuesday, January 19th at 12:15, North Building 100
		\item Summary statistics
		\item PCA
		\item Hierarchical clustering
	\end{itemize}
\end{frame}
%
\begin{frame}[c]\frametitle{Reminder: Files and Software}
	Files:
	\begin{itemize}
		\item Normalized data (from previous module)
		\item Key file with:
		\begin{itemize}
			\item Sample IDs
			\item Subject IDs
			\item Time points
			\item Phenotype
			\item Covariates of interest
			\item Batch IDs
		\end{itemize}
	\end{itemize}
	
	\vspace{4mm}
	Software:
	
	\begin{itemize}
		\item R/RStudio
		\item R packages: 
		\item Git/Bitbucket
		\item Cygwin (Windows users only)
	\end{itemize}
\end{frame}
%
\iffalse
\section{ARI}
%
\subsection{ARI}
%
\begin{frame}[t]\frametitle{ARI}
	\imp{Summary:}\footnote{Science Translational Medicine 2016.}
	\begin{itemize}
		\item Host gene expression profiles distinguish non-infectious from infectious illness and bacterial from viral causes of Acute Respiratory Infection (ARI).
		\item Create opportunities for novel diagnostic platforms to combat inappropriate antibiotic use and antibiotic resistance.
	\end{itemize}
	
	\vspace{2mm}
	\imp{Data:}
	\begin{itemize}
		\item $N=273$ samples.
		\item $p=22,277$ genes (HG-U133A 2.0 microarrays).
		\item 2 Batches (156 and 124 samples each).
		\item 3 Groups: 73 bacterial, 117 viral and 90 SIRS.
	\end{itemize}
	
	\vspace{2mm}
	\imp{Methods}:
	\begin{itemize}
		\item Preprocessing: gcrma normalization.
		\item Batch effects correction: Bayesian fixed effects model.
		\item Classification: sparse logistic regression.
	\end{itemize}
\end{frame}
%
\begin{frame}[t]\frametitle{ARI}
	\imp{Results:}
	\begin{itemize}
		\item 121 gene signature.
		\item 87\% (238/173) overall leave-one-out accuracy.
		\item More accurate than procalcitonin (78\%) and 3 published classifiers (78-83\%).
		\item External validation on 5 public datasets (AUC 0.90-0.99).
	\end{itemize}
	
	\begin{columns}[c]
		\column{0.5\textwidth}
		\begin{center}
			\begin{psfrags}
				\includegraphics[scale=0.28]{./images/pred_tri_unpruned}
			\end{psfrags}
		\end{center}
		\column{0.5\textwidth}
		\begin{center}
			\begin{tabular}{c|ccc}
				\tiny{True/Pred} & bacterial & viral & sirs \\
				\hline
				bacterial & 82.8 & 12.8 & 4.2 \\
				viral & 3.4 & 90.4 & 6.0 \\
				sirs & 9.0 & 4.5 & 86.3
			\end{tabular}
		\end{center}
	\end{columns}
	
	\vspace{2mm}
	\begin{columns}[b]
		\scriptsize
		\column{0.5\textwidth}
		\centering
		Leave-one-out predictions
		\column{0.5\textwidth}
		\centering
		Confusion matrix
	\end{columns}	
\end{frame}
%
\section{NAFLD}
%
\subsection{NAFLD}
%
\begin{frame}[t]\frametitle{NAFLD}
	\imp{Summary:}\footnote{The liver meeting 2015.}
	\begin{itemize}
		\item Metabolomic profiles predict advanced hepatic fibrosis in patients with Non-alcoholic Fatty Liver Disease (NAFLD).
		\item Non-invasive, blood-based biomarkers to accurately stage fibrosis and stratify clinical risk are needed.
	\end{itemize}
	
	\vspace{2mm}
	\imp{Data:}
	\begin{itemize}
		\item $N=200$ samples.
		\item $p=790$ metabolites (ultrahigh performance LC-MS and GC-MS).
		\item 7 Batches (approx. 28 per batch).
		\item Fibrosis stage: 0-4 (ordinal).
	\end{itemize}
	
	\vspace{2mm}
	\imp{Methods}:
	\begin{itemize}
		\item Preprocessing: log-transform.
		\item Batch effects correction: Bayesian fixed effects model.
		\item Ordinal regression: sparse max-margin rank-likelihood regression\footnote{ICML 2015.}.
	\end{itemize}
\end{frame}
%
\begin{frame}[t]\frametitle{NAFLD}
	\imp{Results:}
	\begin{itemize}
		\item 69 metabolite signature.
		\item 0.75 leave-one-out AUC (0-2 vs. 3-4).
		\item More accurate than 5 non-invasive biomarkers (AUC 0.60-0.68).
		\item External validation in progress.
	\end{itemize}
	
	\begin{columns}[c]
		\column{0.5\textwidth}
		\begin{center}
			\begin{psfrags}
				\includegraphics[scale=0.28]{./images/cm_annotated_batch}
			\end{psfrags}
		\end{center}
		\column{0.5\textwidth}
		\begin{center}
			\begin{psfrags}
				\includegraphics[scale=0.27]{./images/roc_012v34}
			\end{psfrags}
		\end{center}
	\end{columns}
	
	\vspace{2mm}
	\begin{columns}[b]
		\scriptsize
		\column{0.5\textwidth}
		\centering
		Confusion matrix
		\column{0.5\textwidth}
		\centering
		ROC curves: 0-2 vs. 3-4
	\end{columns}	
\end{frame}
%
\section{AGP}
%
\subsection{AGP}
%
\begin{frame}[t]\frametitle{AGP}
	\imp{Summary:}\footnote{Work in progress.}
	\begin{itemize}
		\item Microbiome analysis using next generation-sequencing reveals association of microbial profiles with Inflammatory Bowel Disease (IBD) and gluten/lactose intolerance.
		\item Microbiome studies aim to understand the involvement of microbes in health and disease processes.
	\end{itemize}
	
	\vspace{2mm}
	\imp{Data:}
	\begin{itemize}
		\item $N=2,291$ fecal samples from the American Gut Project (AGP).
		\item $p=11,308$ OTUs (NGS).
		\item 213 clinical variables: IBD, lactose intolerance, gluten intolerance, etc.
	\end{itemize}
	
	\vspace{2mm}
	\imp{Methods}:
	\begin{itemize}
		\item Preprocessing: normalized counts.
		\item Model: multi-label supervised Deep Poisson Factor Analysis (DPFA)\footnote{NIPS 2015.}.
	\end{itemize}
\end{frame}
%
\begin{frame}[t]\frametitle{AGP}
	\imp{Results:}
	\begin{itemize}
		\item 2 microbial profiles (topics: T77 and T81) associated with IBD.
		\item 0.67 2-fold cross-validated AUC.
	\end{itemize}
	
	\vspace{2mm}
	\begin{center}
		\begin{psfrags}
			\includegraphics[scale=0.18]{./images/res_agp_disc1}
			\includegraphics[scale=0.18]{./images/res_agp_topic1}
		\end{psfrags}
	\end{center}
	
	\vspace{2mm}
	\centering\scriptsize
	\begin{tabular}{l}
		T77\\\hline
		k-Bacteria; p-Firmicutes; c-Bacilli; o-Lactobacillales; f-Streptococcaceae; g-Streptococcus; s-\\
		k-Bacteria; p-Proteobacteria; c-Gammaproteobacteria; o-Pasteurellales; f-Pasteurellaceae; g-Haemophilus; s-parainfluenzae\\
		k-Bacteria; p-Firmicutes; c-Bacilli; o-Lactobacillales; f-Streptococcaceae; g-Streptococcus; s-\\
		k-Bacteria; p-Firmicutes; c-Clostridia; o-Clostridiales; f-Veillonellaceae; g-Veillonella; s-dispar\\
		k-Bacteria; p-Firmicutes; c-Bacilli; o-Lactobacillales; f-Leuconostocaceae; g-; s-
	\end{tabular}
	
	\vspace{2mm}
	\begin{tabular}{l}
		T81\\\hline
		k-Bacteria; p-Bacteroidetes; c-Bacteroidia; o-Bacteroidales; f-Rikenellaceae; g-; s-\\
		k-Bacteria; p-Firmicutes; c-Clostridia; o-Clostridiales; f-Ruminococcaceae; g-Ruminococcus; s-\\
		k-Bacteria; p-Firmicutes; c-Clostridia; o-Clostridiales; f-Lachnospiraceae; g-Blautia; s-\\
		k-Bacteria; p-Bacteroidetes; c-Bacteroidia; o-Bacteroidales; f-[Odoribacteraceae]; g-Odoribacter; s-\\
		k-Bacteria; p-Firmicutes; c-Clostridia; o-Clostridiales; f-Ruminococcaceae; g-; s-
	\end{tabular}
\end{frame}
\fi
%
\end{document}
